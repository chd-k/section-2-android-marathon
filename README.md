# Section 2 Android Marathon

Task "Classes" for VibeLab

1. [Data classes](https://gitlab.com/chd-k/section-2-android-marathon/-/tree/main/Data%20classes)
1. [Smart casts](https://gitlab.com/chd-k/section-2-android-marathon/-/tree/main/Smart%20casts)
1. [Sealed classes](https://gitlab.com/chd-k/section-2-android-marathon/-/tree/main/Sealed%20classes)
1. [Rename on import](https://gitlab.com/chd-k/section-2-android-marathon/-/tree/main/Rename%20on%20import)
1. [Extension functions](https://gitlab.com/chd-k/section-2-android-marathon/-/tree/main/Extension%20functions)

